﻿USE practica2;

-- 1. Indicar el número de ciudades que hay en la tabla ciudades 

  SELECT 
    COUNT(*)numCiudades 
  FROM 
    ciudad c;

-- 2. Indicar el nombre de las ciudades que tengan una población por encima de la población media
  
  SELECT 
    c.nombre 
  FROM 
    ciudad c 
  WHERE 
    c.población>(
      SELECT 
        AVG(c1.población) 
      FROM 
        ciudad c1);

-- 3. Indicar el nombre de las ciudades que tengan una población por debajo de la población media 

  SELECT 
    c.nombre 
  FROM 
    ciudad c 
  WHERE 
    c.población<(
      SELECT 
        AVG(c1.población) 
      FROM 
        ciudad c1);

-- 4. Indicar el nombre de la ciudad con la población máxima 

  SELECT 
    c.nombre 
  FROM 
    ciudad c 
  WHERE 
    c.población=(
      SELECT 
        MAX(c1.población) 
      FROM 
        ciudad c1);

-- 5. Indicar el nombre de la ciudad con la población mínima 

  SELECT 
    c.nombre 
  FROM 
    ciudad c 
  WHERE 
    c.población=(
      SELECT 
        MIN(c1.población) 
      FROM 
        ciudad c1);

-- 6. Indicar el número de ciudades que tengan una población por encima de la población media 

  SELECT 
    COUNT(*)ciudadesPoblacionMayorMedia 
  FROM 
    ciudad c 
  WHERE 
    c.población>(
      SELECT 
        AVG(c1.población) 
      FROM 
        ciudad c1);

-- 7. Indicarme el número de personas que viven en cada ciudad 

  SELECT 
    p.ciudad, COUNT(*)numPersonas 
  FROM 
    persona p 
  GROUP BY 
    p.ciudad;

-- 8. Utilizando la tabla trabaja indicarme cuantas personas trabajan en cada una de las compañias
  
  SELECT 
    t.compañia, COUNT(*)trabajadores 
  FROM 
    trabaja t 
  GROUP BY 
    t.compañia; 

-- 9. Indicarme la compañía que más trabajadores tiene 

  -- Listamos los trabajadores que tiene cada compañia
  SELECT 
    t.compañia, COUNT(*)trabajadores 
  FROM 
    trabaja t 
  GROUP BY 
    t.compañia;

  -- Sacamos el maximo de trabajadores 
  SELECT 
    MAX(c1.trabajadores) 
  FROM (
    SELECT 
      t.compañia, COUNT(*)trabajadores 
    FROM 
      trabaja t 
    GROUP BY 
      t.compañia)c1;

-- completa
  SELECT 
    t.compañia, COUNT(*)trabajadores 
  FROM 
    trabaja t 
  GROUP BY 
    t.compañia
  HAVING 
    trabajadores=(  
      SELECT 
        MAX(c1.trabajadores) 
      FROM (
        SELECT 
          t.compañia, COUNT(*)trabajadores 
        FROM 
          trabaja t 
        GROUP BY 
          t.compañia)c1);

-- 10. Indicarme el salario medio de cada una de las compañias 

  SELECT 
    t.compañia, AVG(t.salario)salarioMedio 
  FROM 
    trabaja t 
  GROUP BY 
    t.compañia;

-- 11. Listarme el nombre de las personas y la población de la ciudad donde viven 

  SELECT 
    p.nombre, c.población 
  FROM 
    persona p JOIN ciudad c 
  ON 
    p.ciudad = c.nombre;

-- 12. Listar el nombre de las personas, la calle donde vive y la población de la ciudad donde vive 

  SELECT 
    p.nombre, p.calle, c.población 
  FROM 
    persona p JOIN ciudad c 
  ON 
    p.ciudad = c.nombre;

-- 13. Listar el nombre de las personas, la ciudad donde vive y la ciudad donde está la compañía para la que trabaja 

  SELECT p.nombre, p.ciudad AS ciudadVive, c.ciudad AS ciudadTrabaja 
    FROM persona p 
    JOIN trabaja t ON p.nombre = t.persona 
    JOIN compañia c ON t.compañia = c.nombre;

-- 14. Realizar el algebra relacional y explicar la siguiente consulta 

 SELECT p.nombre 
  FROM persona p, trabaja t, compañia c 
  WHERE p.nombre= t.persona 
  AND t.compañia = c.nombre 
  AND c.ciudad = p.ciudad 
  ORDER BY p.nombre;

-- 15. Listarme el nombre de la persona y el nombre de su supervisor 

  SELECT s.persona, s.supervisor 
    FROM supervisa s;

-- 16. Listarme el nombre de la persona, el nombre de su supervisor y las ciudades donde residen cada una de ellos 

  SELECT 
    s.persona,
    p.ciudad AS ciudad_persona, 
    s.supervisor,
    p1.ciudad AS ciudad_s 
  FROM persona p 
  JOIN supervisa s ON p.nombre = s.persona 
  JOIN persona p1 ON s.supervisor = p1.nombre; 

-- 17. Indicarme el número de ciudades distintas que hay en la tabla compañía 

  -- Subconsulta C1
  -- Lista de las ciudades de la tabla compañia
  SELECT DISTINCT c.ciudad 
    FROM compañia c;

  -- Consulta final
  SELECT COUNT(*) numCiudades 
    FROM (
      SELECT DISTINCT c.ciudad 
        FROM compañia c) c1;

-- 18. Indicarme el número de ciudades distintas que hay en la tabla personas 

  -- Subconsulta C1
  -- Lista de las ciudades de la tabla persona
  SELECT DISTINCT p.ciudad 
    FROM persona p;

  -- Consulta final
  SELECT COUNT(*) numCiudades 
    FROM (
      SELECT DISTINCT p.ciudad 
        FROM persona p) c1;

-- 19. Indicarme el nombre de las personas que trabajan para FAGOR 

  SELECT t.persona 
    FROM trabaja t 
    WHERE t.compañia='FAGOR';

-- 20. Indicarme el nombre de las personas que no trabajan para FAGOR 

  -- Subconsulta C1
  -- Personas que trabajan en FAGOR
  SELECT t.persona 
    FROM trabaja t 
    WHERE t.compañia='FAGOR';

  -- Consulta final
  SELECT p.nombre 
    FROM persona p 
      WHERE p.nombre NOT IN (
        SELECT t.persona 
          FROM trabaja t 
          WHERE t.compañia='FAGOR'
      );

-- 21. Indicarme el número de personas que trabajan  para INDRA 

  SELECT COUNT(*)trabajadores 
    FROM trabaja t 
    WHERE t.compañia='INDRA'; 

-- 22. Indicarme el nombre de las personas que trabajan para FAGOR o para INDRA 

  -- Subconsulta C1
  -- Nombre de los que trabajan en Fagor
  SELECT t.persona 
    FROM trabaja t 
    WHERE t.compañia = 'FAGOR';

  -- Subconsulta C2
  -- Nombre de los que trabjan en Indra
  SELECT t.persona 
    FROM trabaja t 
    WHERE t.compañia = 'INDRA';

  -- Consulta final
  SELECT t.persona 
    FROM trabaja t 
    WHERE t.compañia = 'FAGOR'
  UNION
  SELECT t.persona 
    FROM trabaja t 
    WHERE t.compañia = 'INDRA';

-- 23. Listar la población donde vive cada persona, sus salario, su nombre y la compañía para la que trabaja. Ordenar la salida por nombre de la persona y por salario de forma descendente. 

  SELECT c.población, t.salario, p.nombre, t.compañia 
    FROM ciudad c 
    JOIN persona p ON c.nombre = p.ciudad 
    JOIN trabaja t ON p.nombre = t.persona
    ORDER BY t.salario DESC, p.nombre DESC;