﻿/* creando las bases de datos */
DROP DATABASE IF EXISTS practica2;
CREATE DATABASE practica2;
USE practica2;

CREATE TABLE Ciudad (
  nombre Varchar(30) PRIMARY KEY,
  población Integer
);

CREATE TABLE Persona (
  nombre Varchar(30) PRIMARY KEY,
  calle Varchar(30),
  ciudad Varchar(30)
 );

CREATE TABLE Compañia (
  nombre Varchar(30) PRIMARY KEY,
  ciudad Varchar(30)
);

CREATE TABLE Trabaja (
  persona Varchar(30) PRIMARY KEY,
  compañia Varchar(30),
  salario Integer
);

CREATE TABLE Supervisa (
  supervisor Varchar(30),
  persona Varchar(30),
  PRIMARY KEY (supervisor,persona)
);

/**
  Añadiendo las Foreign Key
*/
ALTER TABLE Persona 
  ADD CONSTRAINT fkPersonaCiudad FOREIGN KEY (ciudad) 
      REFERENCES Ciudad(nombre);

ALTER TABLE Compañia
  ADD CONSTRAINT fkCompañiaCiudad FOREIGN KEY (ciudad)
      REFERENCES Ciudad(nombre);

ALTER TABLE Trabaja
  ADD CONSTRAINT fkTrabajaPersona FOREIGN KEY (persona)
      REFERENCES Persona (nombre),
  ADD CONSTRAINT fkTrabajaCompañia FOREIGN KEY (compañia)
      REFERENCES Compañia (nombre);

ALTER TABLE Supervisa
  ADD CONSTRAINT fkSupervisaSupervisor FOREIGN KEY (supervisor) 
      REFERENCES Persona (nombre),
  ADD CONSTRAINT fkSupervisaPersona FOREIGN KEY (persona) 
      REFERENCES Persona (nombre)